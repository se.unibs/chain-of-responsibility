package it.unibs.ing.se.cor;

public class ComicBook extends MediaResource {
    private final String topicBy;
    private final String screenplayBy;
    private final String coverBy;

    public ComicBook(String title, String topicBy, String screenplayBy, String coverBy) {
        super(title);
        this.topicBy = topicBy;
        this.screenplayBy = screenplayBy;
        this.coverBy = coverBy;
    }

    public String getTopicBy() {
        return topicBy;
    }

    public String getScreenplayBy() {
        return screenplayBy;
    }

    public String getCoverBy() {
        return coverBy;
    }
}
