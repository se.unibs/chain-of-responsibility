package it.unibs.ing.se.cor;

import java.util.LinkedList;
import java.util.List;

public class Bookshelf {
    private final List<MediaResource> resources;

    public Bookshelf() {
        resources = new LinkedList<>();
    }

    public Iterable<MediaResource> getResources() {
        return resources;
    }

    public void add(MediaResource resource) {
        resources.add(resource);
    }
}
