package it.unibs.ing.se.cor;

public class MediaResource {
    private final String title;

    public MediaResource(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }
}
