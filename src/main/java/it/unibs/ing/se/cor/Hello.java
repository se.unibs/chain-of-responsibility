package it.unibs.ing.se.cor;

public class Hello {
    public String sayHelloTo(String target) {
        return String.format("Hello, %s!", target);
    }

    public String sayHello() {
        return sayHelloTo("World");
    }
}
