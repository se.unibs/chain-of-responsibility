package it.unibs.ing.se.cor;

public class ResourceController {
    private final Bookshelf bookshelf;

    public ResourceController(Bookshelf bookshelf) {
        this.bookshelf = bookshelf;
    }

    /**
     * L'invocazione del presente metodo deve restituire una stringa composta da più righe, separate dal carattere \n
     * Ciascuna riga conterrà la descrizione di una delle risorse presenti nello scaffale (bookshelf.getResources()):
     * per ogni tipo di risorsa verranon visualizzate informazioni diverse
     * @return
     */
    public String renderResources() {
        return "";
    }
}
