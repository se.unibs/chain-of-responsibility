package it.unibs.ing.se.cor;

public class Film extends MediaResource {
    private final String company;
    private final String director;
    private final int year;

    public Film(String title, String company, String director, int year) {
        super(title);
        this.company = company;
        this.director = director;
        this.year = year;
    }

    public String getCompany() {
        return company;
    }

    public String getDirector() {
        return director;
    }

    public int getYear() {
        return year;
    }
}
