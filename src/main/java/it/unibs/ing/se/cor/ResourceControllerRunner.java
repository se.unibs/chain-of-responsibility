package it.unibs.ing.se.cor;

public class ResourceControllerRunner {
    public static void main(String[] args) {
        var shelf = new Bookshelf();
        shelf.add(new Film("L'attimo fuggente", "Touchstone Pictures",
                "Peter Weir", 1989));
        shelf.add(new Book("Mr Gwyn", "Feltrinelli", "Alessandro Baricco"));
        shelf.add(new Book("Refactoring", "Addison Wesley", "Martin Fowler"));
        shelf.add(new Film("Harry Potter e la pietra filosofale", "Warner Bros Pictures",
                "Chris Columbus", 2001));
        shelf.add(new ComicBook("Nathan Never #50 - La biblioteca di Babele", "Katia Albini & Antonio Serra",
                "Antonio Serra", "Claudio Castellini"));
        var controller = new ResourceController(shelf);
        var description = controller.renderResources();
        System.out.println("=================");
        System.out.println(description);
        System.out.println("=================");
    }
}
