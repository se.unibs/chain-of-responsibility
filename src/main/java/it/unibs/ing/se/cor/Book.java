package it.unibs.ing.se.cor;

import java.util.Arrays;

public class Book extends MediaResource {
    private final String publishingHouse;
    private final Iterable<String> authors;

    public Book(String title, String publishingHouse, String ... authors) {
        super(title);
        this.publishingHouse = publishingHouse;
        this.authors = Arrays.asList(authors);
    }

    public String getPublishingHouse() {
        return publishingHouse;
    }

    public Iterable<String> getAuthors() {
        return authors;
    }
}
