package it.unibs.ing.se.cor;

import org.junit.jupiter.api.Test;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class HelloTest {
    @Test
    public void shouldSayHelloToProvidedTarget() {
        assertThat(new Hello().sayHelloTo("Pietro"), is(equalTo("Hello, Pietro!")));
    }

    @Test
    public void shouldSayHelloToDefaultTarget() {
        assertThat(new Hello().sayHello(), is(equalTo("Hello, World!")));
    }
}
