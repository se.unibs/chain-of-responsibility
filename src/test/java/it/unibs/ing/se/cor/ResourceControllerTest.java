package it.unibs.ing.se.cor;

import org.junit.jupiter.api.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;


public class ResourceControllerTest {
    @Test
    public void assertTrueIsNotFalse() {
        assertThat(true, is(not(false)));
    }

//    @Test
//    public void shouldRenderSingleBookCorrectly() {
//        var book = new Book("Mr Gwyn", "Feltrinelli", "Alessandro Baricco");
//        var shelf = new Bookshelf();
//        shelf.add(book);
//        var controller = new ResourceController(shelf);
//        var description = controller.renderResources();
//        assertThat(description, is(equalTo("[Book] Mr Gwyn, by Alessandro Baricco (Feltrinelli)")));
//    }
//
//    @Test
//    public void shouldRenderSingleComicBookCorrectly() {
//        var comicBook = new ComicBook("Nathan Never #50 - La biblioteca di Babele", "Katia Albini & Antonio Serra",
//                "Antonio Serra", "Claudio Castellini");
//        var shelf = new Bookshelf();
//        shelf.add(comicBook);
//        var controller = new ResourceController(shelf);
//        var description = controller.renderResources();
//        assertThat(description, is(equalTo("[ComicBook] Nathan Never # 50 - La biblioteca di Babele, by Antonio Serra. Cover by Claudio Castellini")));
//    }
//
//    @Test
//    public void shouldRenderSingleFilmCorrectly() {
//        var film = new Film("L'attimo fuggente", "Touchstone Pictures",
//                "Peter Weir", 1989);
//        var shelf = new Bookshelf();
//        shelf.add(film);
//        var controller = new ResourceController(shelf);
//        var description = controller.renderResources();
//        assertThat(description, is(equalTo("L'attimo fuggente, by Peter Weir. A Touchstone Pictures' [Film] (1989).")));
//    }
//
//    @Test
//    public void shouldRenderManyResourcesCorrectly() {
//        var shelf = new Bookshelf();
//        shelf.add(new Film("L'attimo fuggente", "Touchstone Pictures",
//                "Peter Weir", 1989));
//        shelf.add(new Book("Mr Gwyn", "Feltrinelli", "Alessandro Baricco"));
//        shelf.add(new Book("Refactoring", "Addison Wesley", "Martin Fowler"));
//        var film = new Film("Harry Potter e la pietra filosofale", "Warner Bros Pictures",
//                "Chris Columbus", 2001);
//        var controller = new ResourceController(shelf);
//        var description = controller.renderResources();
//        assertThat(description, is(equalTo(
//                "L'attimo fuggente, by Peter Weir. A Touchstone Pictures' [Film] (1989).\n" +
//                        "[Book] Mr Gwyn, by Alessandro Baricco (Feltrinelli)\n" +
//                        "[Book] Refactoring, by Martin Fowler (Addison Wesley)\n" +
//                        "Harry Potter e la pietra filosofale, by Chris Columbus. A Warner Bros Pictures' [Film] (2001).")));
//    }
}
